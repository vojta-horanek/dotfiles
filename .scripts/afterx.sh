#!/bin/sh

#monitor calibration
#$HOME/.monicarc

#wallpaper
feh ~/Pictures/wallpaper.png --bg-scale

#set custom keyboard layout
xmodmap .xmodmaprc

#run status loop
sh $HOME/.scripts/status_loop.sh &

#run notification service
dunst &

#compositor
#picom --xrender-sync-fence &

#enable volume and brightness indicators
tail -f $HOME/.scripts/xobpipe | xob -s volume &
tail -f $HOME/.scripts/xobbrightness | xob -s brightness -m 15 &

#set default layout as us
echo -n "us" > $HOME/.scripts/current_layout

xset -b b 0

#set speed on trackpoint
#xinput set-prop "TPPS/2 IBM TrackPoint" "libinput Accel Speed" -0.4
#configuration set in /etc/X11/xorg.conf.d/30-trackpoint.conf
