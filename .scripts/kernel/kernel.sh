#!/bin/sh

FOLDER="$HOME/linux"
FLAGS=""

function vecho {
	if [[ $FLAGS =~ "verbose" ]]; then
		echo $2 "$1"
	fi
}

function print_help {
	echo -e "-f, --force\toverrides already downloaded kernel"
	echo -e "-v, --verbose\tprint more information"
	echo -e "-h, --help\tprint this message and exit"
}

for arg in "$@"
do
	case $arg in
		-f | --force)
			FLAGS="${FLAGS} force"
			;;
		-v | --verbose)
			FLAGS="${FLAGS} verbose"
			;;
		-h | --help)
			print_help
			exit 0
			;;
		*)
			echo "Unrecognized option \"$arg\""
			exit 2
			;;
	esac
done

vecho "Getting latest version... " -ne

SOURCE=$(curl --connect-timeout 10 -s "https://www.kernel.org")

if [ $? -ne 0 ]; then
	echo "Cannot get latest version"
	exit 3
fi

PARSED=$(echo "$SOURCE" | grep -A 1 '<td id="latest_link">' | tail -n 1 | sed -e 's/<a href=\"\(.*\)\">\(.*\)<\/a>/\1#\2/')

URL=$(echo "$PARSED" | cut -d'#' -f 1 | xargs)
VERSION=$(echo "$PARSED" | cut -d'#' -f 2)

vecho "$VERSION"

TMP="$FOLDER/versions/$VERSION"

if [ -d $TMP ]; then
	if [[ $FLAGS =~ "force" ]]; then
		vecho "Removing \"$TMP\""
		rm -rf "$TMP"
		mkdir -p "$TMP"
	else
		echo "Latest version already downloaded in \"$TMP\", use -f to download again"
		exit 0
	fi
else
	mkdir -p "$TMP"
fi

vecho "Downloading version \"$VERSION\" to $TMP.tar.xz"

curl_flag="-s"
if [[ $FLAGS =~ "verbose" ]]; then
	curl_flag="--progress-bar"
fi
curl --connect-timeout 10 $curl_flag -o "$TMP.tar.xz" $URL

if [ $? -ne 0 ]; then
	echo "Cannot download latest version from $URL"
	exit 2
fi

vecho "Extracting $TMP.tar.xz to $TMP"

tar_flag=""
if [[ $FLAGS =~ "verbose" ]]; then
	tar_flag="--totals"
fi
tar xf "$TMP.tar.xz" -C "$TMP" --strip-components=1 $tar_flag

if [ $? -ne 0 ]; then
	echo "Cannot extract $TMP.tar.xz, leaving archived kernel"
	exit 1
fi

vecho "Removing $TMP.tar.xz"
rm "$TMP.tar.xz"

if [ -f "$FOLDER/versions/latest" ]; then
	rm "$FOLDER/versions/latest"
fi

vecho "Creating link $FOLDER/versions/latest -> $TMP"
ln -s "$TMP" "$FOLDER/versions/latest"
