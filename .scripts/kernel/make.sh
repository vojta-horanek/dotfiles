#!/bin/sh

$FOLDER="$HOME/linux"
$CORES=5
$BOOT_FLAVOR="moonlight"

if [ $# -lt 1 ]; then
	echo "usage: $0 [config file] (version)"
	exit 1
fi

if ! [ -f "$1" ]; then
	echo "Config file "$1" does not exist"
	exit 1
fi

if [ $# -eq 2 ]; then
	version="$2"
else
	version="latest"
fi

if ! [ -d "$FOLDER/versions/$version" ]; then
	echo "Cannot find version \"$version\""
	exit 1
fi

echo "Copying version to a temporary location"
mkdir -p "$FOLDER/tmp_build"
cp -r "$FOLDER/versions/$version" "$FOLDER/tmp_build"

echo "Copying config file to build folder"
cp "$1" "$FOLDER/tmp_build/.config"

time make -j$CORES

echo "Copying bzImage to /boot directory"

