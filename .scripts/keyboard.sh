#!/bin/sh

read -r layout < $HOME/.scripts/current_layout
if [ "$layout" = "us" ]; then
	echo "cz" > $HOME/.scripts/current_layout
	setxkbmap -layout cz -variant qwerty
else
	echo "us" > $HOME/.scripts/current_layout
	setxkbmap -layout us
fi	

xmodmap $HOME/.xmodmaprc
