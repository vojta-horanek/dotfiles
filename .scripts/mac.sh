#!/bin/sh
path="$HOME/.cache/uzl_mac.txt"
if [ $# -lt 2 ]; then
	echo "Usage: uzmac [room] [computer]"
	echo "room can be either a number or \"Šatna\""
	exit 1
fi

curl https://wiki.uzlabina.eu/wiki/%C5%A0ablona:MAC_adresy?action=raw --silent -o $path
if [ $? -ne 0 ]; then
	if ! [ -f $path ]; then
		echo "Table is not cached and cannot be downloaded from the internet. Please connect first."
		exit 1
	fi
fi

mac=$(sed "1,3d;s/^..//;/^$/d;s/\[\[//g;s/\]\]//g;s/||/|/g;s/ |/|/g;s/| /|/g;s/Šatna/satna/g" $path | grep "|$1|$2|" | cut -d '|' -f 5);

if [ $? -ne 0 ] || [ "$mac" = "" ]; then
	echo "No MAC address found in table"
	exit 1
fi

if [ $# -eq 3 ]; then
	device="$3"
else
	device="enp0s25"
fi
echo "changing to mac $mac"
macchanger -m "$mac" $device
