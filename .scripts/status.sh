#!/bin/sh

WLAN="wlan0"
ETH="enp0s25"

battery="$(cat /sys/devices/platform/smapi/BAT0/remaining_percent)%"
date="$(date +'%d-%m-%Y %R')"
temp="$(sensors coretemp-isa-0000 | grep Package | cut -d' ' -f5 | sed 's/+//g')"
keyboard="$(cat $HOME/.scripts/current_layout)"

battery_time="$(cat /sys/devices/platform/smapi/BAT0/remaining_running_time_now)"
if ! [ "$(cat /sys/class/power_supply/BAT0/status)" = "Discharging" ]; then
	battery="$battery [AC]"
	battery_time="$(cat /sys/devices/platform/smapi/BAT0/remaining_charging_time)"
fi

if [ "$(cat /sys/class/net/$ETH/operstate)" = "up" ]; then
	ip="$(ip -o -4 addr show dev $ETH | cut -d' ' -f7 | cut -d'/' -f1)"
	net="$ETH: "
elif [ "$(cat /sys/class/net/$WLAN/operstate)" = "up" ]; then
	net="$(iwgetid -r $WLAN): "
	ip="$(ip -o -4 addr show dev $WLAN | cut -d' ' -f7 | cut -d'/' -f1)"
else
	ip=""
	net="not connected"
fi

echo -n "$keyboard | $net$ip | $temp | $battery ($battery_time) | $date"
