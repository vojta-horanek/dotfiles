#!/bin/sh

if [ "$1" = "up" ]; then
	pactl set-sink-volume 0 +5%
	echo "$(pulsemixer --get-volume | cut -f1 -d' ')" > $HOME/.scripts/xobpipe
elif [ "$1" = "down" ]; then
	pactl set-sink-volume 0 -5%
	echo "$(pulsemixer --get-volume | cut -f1 -d' ')" > $HOME/.scripts/xobpipe
elif [ "$1" = "mute" ]; then
	pactl set-sink-mute 0 toggle
elif [ "$1" = "mutemic" ]; then
	pactl set-source-mute 1 toggle
fi
