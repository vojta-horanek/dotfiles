#!/bin/sh

su_app=pkexec

if [ -t 0 ]; then
	su_app=sudo
fi

if [ -f /sys/class/net/wgcf/operstate ]; then
	if ! [ -t 0 ]; then
		notify-send WARP "Turning off" -i network-vpn
	fi
#	nmcli con down id "David VPN"
	$su_app /usr/bin/wg-quick down /home/vojta/.wgcf/wgcf.conf 
else
	if ! [ -t 0 ]; then
		notify-send WARP "Turning on" -i network-vpn
	fi
#	nmcli con up id "David VPN"
	sh /home/vojta/.scripts/wgcf.sh silent
	$su_app /usr/bin/wg-quick up /home/vojta/.wgcf/wgcf.conf 
fi
