#!/usr/bin/env bash
set -euo pipefail
shopt -s inherit_errexit 2>/dev/null || true

# this script will generate wg-quick(8) config file
# in order to connect to Cloudflare Warp using Wireguard on Linux
# note: this is *absolutely not* an official client from Cloudflare

# Copyright (C) 2019 Ondrej Caletka
# based heavily on macOS client by Jay Freeman (saurik)
# Original source: https://cache.saurik.com/twitter/wgcf.sh

# Zero Clause BSD license {{{
#
# Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
# }}}

if ! which jq >/dev/null || ! which wg >/dev/null; then
    echo "Please install jq and wireguard-tools"
    exit 0
fi

mkdir -p ~/.wgcf
chmod 700 ~/.wgcf
prv=~/.wgcf/private.key
usr=~/.wgcf/identity.cfg
cnf=~/.wgcf/wgcf.conf

pub=$({ cat "${prv}" 2>/dev/null || wg genkey | tee "${prv}"; } | wg pubkey)
test -n "${pub}"

api=https://api.cloudflareclient.com/v0i1909051800
ins() { vrb=$1; shift; curl -s -H 'user-agent:' -H 'content-type: application/json' -X "${vrb}" "${api}/$@"; }
sec() { ins "$@" -H 'authorization: Bearer '"${reg[1]}"''; }

cfgjson=$(if [[ -e "${usr}" ]]; then
    reg=($(cat "${usr}"))
    test "${#reg[@]}" -eq 2
    sec GET "reg/${reg[0]}"
else
    reg=($(ins POST "reg" -d '{"install_id":"","tos":"'"$(date -u +%FT%T.000Z)"'","key":"'"${pub}"'","fcm_token":"","type":"ios","locale":"en_US"}' |
        jq -r '.result|.id+" "+.token'))
    test "${#reg[@]}" -eq 2
    echo "${reg[@]}" >"${usr}"
    sec PATCH "reg/${reg[0]}" -d '{"warp_enabled":true}'
    fi)
# echo $cfgjson
cfg=($(echo $cfgjson| jq -r '.result.config|(.peers[0]|.public_key+" "+.endpoint.host)+" "+(.interface.addresses|.v4+" "+.v6)'))
test "${#cfg[@]}" -eq 4

cat >"$cnf" <<EOF
[Interface]
PrivateKey = $(cat "${prv}")
DNS = 1.1.1.1
Address = ${cfg[2]}
Address = ${cfg[3]}
MTU = 1400
# This fixes up IPv6 depreference due to the use of unique local addressing
Address = ${cfg[3]/#fd01:5ca1:/2001:db8:}
PostUp = ip6tables -t nat -I POSTROUTING 1 -o %i -j SNAT --to-source ${cfg[3]}
PreDown = ip6tables -t nat -D POSTROUTING 1

[Peer]
PublicKey = ${cfg[0]}
AllowedIPs = 0.0.0.0/0
AllowedIPs = ::/0
Endpoint = ${cfg[1]}
EOF

if [ "$#" -eq 0 ]; then
	echo "To connect, run as root:"
	echo "# wg-quick up ${cnf}"
	echo ""
	echo "To disconnect, run as root:"
	echo "# wg-quick down ${cnf}"
fi
