set nocompatible
syntax enable 
filetype indent plugin on
set number
set hidden
set wildmenu
set showcmd
set ignorecase
set smartcase
set autoindent
set ruler
set backspace=indent,eol,start
set mouse=a
set path+=**
set wildmenu
command! MakeTags !ctags -R .
